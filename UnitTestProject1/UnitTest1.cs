﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using WinPlay;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void Test_GMemberMethod()
        {
            GroupMember gm = new GroupMember("Ivan", "Kharchevka", "Alexandrovich", "0394800000", "Key");
            string res = gm.GetMemberGeneralInfo();
            Assert.AreEqual(res, "Name: Kharchevka Ivan Alexandrovich ID Code: 0394800000 Position: Key");
        }
        [TestMethod]
        public void Test_GroupMethod()
        {
            Group g = new Group("IMAGINE DRAGONS");
            string res = g.GetGroup();
            Assert.AreEqual(res, "Group name: IMAGINE DRAGONS");
        }
        [TestMethod]
        public void Test_TitleMethod()
        {
            Title t = new Title("BELIEVER");
            string res = t.GetTitle();
            Assert.AreEqual(res, "Title name: BELIEVER");
        }
    }
}
